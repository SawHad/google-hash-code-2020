import java.io.*;
import java.util.*;

public class Parser{

	public static int partition(int arr[], int begin, int end, Univers toto)
	{
		int pivot = arr[end];
		int i = (begin-1);

		for (int j = begin; j < end; j++) {
			if (toto.list_book[arr[j]] <= toto.list_book[pivot]) {
				i++;

				int swapTemp = arr[i];
				arr[i] = arr[j];
				arr[j] = swapTemp;
			}
		}

		int swapTemp = arr[i+1];
		arr[i+1] = arr[end];
		arr[end] = swapTemp;

		return i+1;
	}
	public static void quick_sort_score(int arr[], int begin, int end, Univers toto)
	{
		if (begin < end) {
			int partitionIndex = partition(arr, begin, end, toto);

			quick_sort_score(arr, begin, partitionIndex-1, toto);
			quick_sort_score(arr, partitionIndex+1, end, toto);
		}
	}
	public static void array_reverse(int[] array)
	{
		for(int i=0; i<array.length/2; i++){
			int temp = array[i];
			array[i] = array[array.length -i -1];
			array[array.length -i -1] = temp;
		}
	}

	public static Univers parse(String p){
		File fichier = new File(p);
		Scanner scanner;
		try{
			scanner = new Scanner(fichier);
			String line = scanner.nextLine();
			String [] l1 = line.split(" ");
			int nb_livres = Integer.parseInt(l1[0]);
			int nb_lib = Integer.parseInt(l1[1]);
			int nb_jour = Integer.parseInt(l1[2]);
			int compt = 0;
			line = scanner.nextLine();
			l1 = line.split(" ");
			int [] list_livre = new int[nb_livres];
			for(int i = 0; i < nb_livres; i++){
				list_livre[i] = Integer.parseInt(l1[i]);
			}
			Univers tot = new Univers(nb_jour, list_livre);
			LinkedList<Lib> res = new LinkedList<Lib>();
			while(compt != nb_lib){
				line = scanner.nextLine();
				l1 = line.split(" ");
				Lib ne = new Lib(compt, Integer.parseInt(l1[0]), Integer.parseInt(l1[1]), Integer.parseInt(l1[2]));
				line = scanner.nextLine();
				l1 = line.split(" ");
				int [] tab_livre = new int[ne.nb_books];
				for(int i = 0; i < ne.nb_books; i++){
					tab_livre[i] = Integer.parseInt(l1[i]);
				}
				quick_sort_score(tab_livre, 0, ne.nb_books - 1, tot);
				array_reverse(tab_livre);
				ne.add_books(tab_livre);
				res.add(ne);
				compt ++;
			}
			tot.add_list_book(res);
			return tot;
		}catch(Exception e){
			System.out.println("error "+e);
		}
		return null;
	}
}
