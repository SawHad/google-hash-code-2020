class Lib
{
	int id;
	int nb_books;
	int signup_time;
	int books_per_day;
	int[] tab_content; // les livres de la librairie

	public Lib(int id, int nb_books, int signup_time, int books_per_day)
	{
		this.id = id;
		this.nb_books = nb_books;
		this.signup_time = signup_time;
		this.books_per_day = books_per_day;
	}


	public void add_books(int[] tab){
		this.tab_content = tab.clone();
	}
	public void affiche(){
		System.out.println(nb_books + " " + signup_time + " " +  books_per_day);
		for (int i = 0; i < tab_content.length; i++) {
			System.out.print (tab_content[i] + " ");
		}
		System.out.println();
	}
	
	public String tab_to_string()
	{
		String rep = "";

		for(int i = 0; i < tab_content.length; i++)
		{
			rep += tab_content[i];
			rep += " ";
		}
		return rep;
	}
}
